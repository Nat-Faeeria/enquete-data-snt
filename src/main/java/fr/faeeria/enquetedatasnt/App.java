package fr.faeeria.enquetedatasnt;

import io.github.cdimascio.dotenv.Dotenv;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.sql.*;

@Path("/enquete-data")
public class App {

	@GET
	@Path("/{page}")
	@Produces(MediaType.TEXT_HTML)
	public Response getPage(@PathParam("page") String page) {
		try {
		 InputStream stream = getClass().getClassLoader().getResourceAsStream(page);
		 if (stream.equals(null)) {
             return Response.status(Response.Status.NOT_FOUND).entity("<h1>404 - Page Not Found, Sorry ! </h1>").build();
         }
		 return Response.ok(stream).build();
		} catch (Exception e) {
			e.printStackTrace();
            return Response.status(Response.Status.NOT_FOUND).entity("<h1>Page Not Found, Sorry ! </h1>").build();
        }
	}

	@POST
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.TEXT_PLAIN)
	public Response sendDBRequest(String rawRequest) {
		String result = new String();

		String requestSQL = transformRequestToSQL(rawRequest);

		Dotenv dotenv = Dotenv.configure().directory("/").load();;
		StringBuilder dbUrlBuilder = new StringBuilder();
		dbUrlBuilder.append("jdbc:mysql://")
				.append(dotenv.get("DB_ADDRESS"))
				.append(":").append(dotenv.get("DB_PORT"))
				.append("/").append(dotenv.get("DB_NAME"))
				.append("?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
		String dbUrl = dbUrlBuilder.toString();
		try {
			Connection con = DriverManager.getConnection(
                    dbUrl, dotenv.get("DB_USER"),dotenv.get("DB_PASSWORD"));

			Statement stmt=con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ResultSet rs=stmt.executeQuery(requestSQL);
			int nbColumns = rs.getMetaData().getColumnCount();
			String comma = " , ", retChar = "\n";
			StringBuilder sb = new StringBuilder();
			while (rs.next()) {
				for (int i=1;i<nbColumns;i++) {
					Object o = rs.getObject(i);
					sb.append(getStringFromRS(o));
					sb.append(comma);
				}
				Object o = rs.getObject(nbColumns);
				sb.append(getStringFromRS(o));
				sb.append(retChar);
			}
			result = sb.toString();
			if (result == "") {
				result = "La requête n'a pas renvoyé de résultat !";
			}
		} catch (SQLException e) {
			result = e.getMessage();
		}
		return Response.ok(result).build();
	}

	private String getStringFromRS(Object o) {
		if (o == null) {
			return "NULL";
		} else {
			return o.toString();
		}
	}

	private String transformRequestToSQL(String rawRequest) {
		rawRequest = rawRequest.replaceAll("SELECTIONNER", "SELECT DISTINCT");
		rawRequest = rawRequest.replaceAll("Selectionner", "SELECT DISTINCT");
		rawRequest = rawRequest.replaceAll("selectionner", "SELECT DISTINCT");
		rawRequest = rawRequest.replaceAll("Sélectionner", "SELECT DISTINCT");
		rawRequest = rawRequest.replaceAll("sélectionner", "SELECT DISTINCT");

		rawRequest = rawRequest.replaceAll("DANS", "FROM");
		rawRequest = rawRequest.replaceAll("Dans", "FROM");
		rawRequest = rawRequest.replaceAll("dans", "FROM");

		rawRequest = rawRequest.replaceAll("TELS QUE", "WHERE");
		rawRequest = rawRequest.replaceAll("Tels Que", "WHERE");
		rawRequest = rawRequest.replaceAll("Tels que", "WHERE");
		rawRequest = rawRequest.replaceAll("tels que", "WHERE");

		rawRequest = rawRequest.replaceAll("TRIER PAR", "ORDER BY");
		rawRequest = rawRequest.replaceAll("Trier Par", "ORDER BY");
		rawRequest = rawRequest.replaceAll("Trier par", "ORDER BY");
		rawRequest = rawRequest.replaceAll("trier par", "ORDER BY");

		rawRequest = rawRequest.replaceAll("EST VIDE", "IS NULL");
		rawRequest = rawRequest.replaceAll("Est Vide", "IS NULL");
		rawRequest = rawRequest.replaceAll("Est vide", "IS NULL");
		rawRequest = rawRequest.replaceAll("est vide", "IS NULL");

		rawRequest = rawRequest.replaceAll("JOINTURE", "INNER JOIN");
		rawRequest = rawRequest.replaceAll("Jointure", "INNER JOIN");
		rawRequest = rawRequest.replaceAll("jointure", "INNER JOIN");

		rawRequest = rawRequest.replaceAll("QUAND", "ON");
		rawRequest = rawRequest.replaceAll("Quand", "ON");
		rawRequest = rawRequest.replaceAll("quand", "ON");

		rawRequest = rawRequest.replaceAll("ET", "AND");


		rawRequest = rawRequest.replaceAll("COMME", "LIKE");
		rawRequest = rawRequest.replaceAll("Comme", "LIKE");
		rawRequest = rawRequest.replaceAll("comme", "LIKE");

		return rawRequest;
	}
}
