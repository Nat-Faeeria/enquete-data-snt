#!/bin/bash
# location should contain the location of folder you can share with the web container. This folder must contain grizzly.jar
[ -z "$1" ] && location=$(pwd) || location=$1
[ -z "$2" ] && password="BadPassword" || password=$2
echo "Searching for .sql and .jar files in "$location
echo "Creating network enquete-data-snt for docker"
docker network create enquete-data-snt
echo "Creating container eds-db with second argument as password"
docker run --network enquete-data-snt --name eds-db -e MYSQL_ROOT_PASSWORD=$password -d mysql:latest
echo "Waiting 30 sec for container eds-db to initialize"
sleep 10s
echo "Creating  database, user and importing data in the database"
docker exec -i eds-db mysql -uroot -p$password < $location/script.sql
echo "Launching the app container eds-web"
docker run -p 80:80 -v $location:/home:ro --network enquete-data-snt -d --name eds-web openjdk:11.0.6-jre /usr/local/openjdk-11/bin/java -jar /home/grizzly.jar